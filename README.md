# Документация Broadcast Public API

## Оглавление
1. [Introduction](#introduction)
2. [Stands](#stands)
3. [Postman](#postman)
4. [Methods](#methods)
    1. [Authorization](#authorization)
        * [/token `POST`](#token-post)
    2. [Games](#games)
        * [/games `GET`](#games-get)
        * [/games/{gameId} `GET`](#gamesgameid-get)
    3. [Данные](#данные)   
        * [/season/{seasonId} `GET`](#seasonseasonid-get)
        * [/league/{leagueId} `GET`](#leagueleagueid-get)
        * [/club/{clubId} `GET`](#clubclubid-get)
        * [/player/{playerId} `GET`](#playerplayerid-get)
    4. [Изображения](#изображения)    
        * [img/{imageId}.png `GET`](#imgimageidpng-get)
    5. [События матча](#cобытия-матча)
        * [/events/{gameId} `GET`](#eventsgameid-get)
        * [/events/{gameId} `POST`](#eventsgameid-get)
        * [/events/{gameId}/{eventId} `DELETE`](#eventsgameideventid-delete)
    6. [РПЛ](#рпл)
        * [/rpl/games `GET`](#rplgames-get)
        * [/rpl/games/{gameId} `GET`](#rplgamesgameid-get)
        * [/rpl/events/{gameId} `GET`](#rpleventsgameid-get)
        * [/rpl/flat/{gameId} `GET`](#rplflatgameid-get)
        * [/rpl/statistic/{gameId} `GET`](#rplstatisticgameid-get)
        

## Introduction
Все успешные ответы сервера, кроме файлов приходят в виде json со схемой

```javascript
{
    "success": true,
    "result": {...}
}
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `success` | `boolean` | true при всех успешных запросах |
| `result` | `object` | основное тело ответа |


Все безуспешные ответы сервера приходят в виде json со схемой

```javascript
{
    "success": false,
    "error": ".."
}
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `success` | `boolean` | false при всех безуспешных запросах |
| `error` | `string` | пояснение к ошибке |

## Stands

Сервис работает в двух экземплярах: тестовый и продакшен.

| Type | Url |
| :--- | :--- | 
| Продакшен | https://broadcast.vsporte.ru/api/v2 |
| Тестовый | https://devbroadcast.vsporte.ru/api/v2 |

## Postman
[Коллекция запросов postman всех методов текущей документации](https://gitlab.com/anovisoft_broadcast_public/broadcast-api-doc/-/blob/main/gzCollection.postman_collection.json)

## Methods
### Authorization
Каждый запрос к апи кроме авторизации и получение изображений, требует наличия в запросе Header Authorization

#### /token `POST`
Получение ключа авторизации 

За логином и паролем можно обратиться к [Новиков Андрей](https://t.me/AnoviSoftAndrew)

```http
POST /token
```

##### Тело POST запроса `form-data` 
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `username` | `string` | Логин пользователя |
| `password` | `string` | Пароль пользователя |

##### Пример ответа
```javascript
{
    "result": {
        "authToken": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJwdWJsaWNfYXBpIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy91c2VyZGF0YSI6IjQ0NzI1YzFlLWJjNzsdFGFhYS1iMDllLTlmMzk1OTdkOWJiOSIsImV4cCI6MTY1NDg3NTQ1MH0.s4ETLA13-xpHOdzyX68zKBVjbQpRxkOOm4imJHVHwQA",
        "expires": "2022-06-10T15:37:30.4286132Z"
    },
    "success": true
}
```
`TokenResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `authToken` | `string` | ключ авторизации. Это значение используется в заголовке(HEADER) `Authorization` |
| `expires` | `string` | Дата время “протухания” ключа. В формате ISO 8601 |


### Games

#### /Games `GET`
Получение списка доступных матчей

```http
GET /games
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": [
        {
            "gameId": "22768d45-a654-4d33-9620-4c8c289edcad",
            "status": 2,
            "dateTime": "2022-05-11T16:00:00.0000000Z",
            "seasonId": "22d56aa8-08dc-4a38-8c9b-bce87cae57e3",
            "season": "2021/2022",
            "leagueId": "03c9d799-d6b6-4216-aa2a-fdda52351800",
            "league": "ФНЛ",
            "clubHostId": "4082238b-9ed2-44ec-a1d2-81446340e34d",
            "clubHost": "Факел",
            "clubGuestId": "16d07540-4620-4551-92bb-158b771f5232",
            "clubGuest": "КАМАЗ"
        }
    ]
}
```

`PublicApiGameListItemResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameId` | `string` | Идентификатор матча |
| `status` | `number` | Статус состояния игры. |
| `dateTime` | `string` | Дата время матча в формате. В формате ISO 8601 |
| `seasonId` | `string` | Идентификатор сезона |
| `season` | `string` | Название сезона |
| `leagueId` | `string` | Идентификатор лиги |
| `league` | `string` | Название лиги |
| `clubHostId` | `string` | Идентификатор клуба хозяев |
| `clubHost` | `string` | Название клуба хозяев |
| `clubGuestId` | `string` | Идентификатор клуба гостей |
| `clubGuest` | `string` | Название клуба гостей |

`status`
| Value | Description |
| :--- | :--- |
| 1 | Матч запланирован |
| 2 | Матч идет |
| 3 | Матч завершен |

#### /games/{gameId} `GET`
Получение списка доступных матчей

##### Пример пути 
```http
GET /games/22768d45-a654-4d33-9620-4c8c289edcad
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": {
        "gameId": "22768d45-a654-4d33-9620-4c8c289edcad",
        "status": 2,
        "dateTime": "2022-05-11 16:00:00Z",
        "seasonId": "22d56aa8-08dc-4a38-8c9b-bce87cae57e3",
        "season": "2021/2022",
        "leagueId": "03c9d799-d6b6-4216-aa2a-fdda52351800",
        "league": "ФНЛ",
        "hostTeam": {
            "clubId": "4082238b-9ed2-44ec-a1d2-81446340e34d",
            "club": "Факел",
            "scheme": "3-4-3",
            "teamColorTop": "#de0000",
            "teamColorBottom": "#0026ff",
            "coach": {
                "name": "Олег",
                "surname": "Василенко",
                "coachType": "head coach"
            },
            "teamPlayers": {
                "captainPlayerId": "32d764fb-b4df-41e7-ab9b-50d52cc04de4",
                "mainTeamPlayerIds": [
                    "4b8ae8ab-84a8-4197-b5eb-a6b627a96d71",
                    "32d764fb-b4df-41e7-ab9b-50d52cc04de4",
                    "f9d1b604-17ad-4712-aa5d-d79590f7d79a",
                    "b489be5c-8f58-48f3-af4d-f5a6abb91cb3",
                    "2224d7f1-8e2b-44f8-a587-c6693dee469e",
                    "3cb793ba-c535-4fbd-ac28-f0e52a0d4668",
                    "47d8feba-d93c-4139-8282-75c1e28b47de",
                    "4cf33b6e-5195-499c-8c63-578c49dbd5d9",
                    "4c38929f-a976-41a3-9be3-4751789ccf48",
                    "7b592526-fbd4-48da-b11a-3dbebee71e60",
                    "c96d1df5-119a-41a5-a282-14863145ff32"
                ],
                "replaceTeamPlayerIds": [
                    "1bcf97f4-825a-4b8e-a7c4-2a990cd03134",
                    "b7098bd0-a719-44d4-9e7e-fe523a1f278e",
                    "cc5d4eb9-adaa-4dac-a982-b906b3f7c7f2",
                    "34b184a5-0cfc-4a88-91fe-f26a0ef9ec67",
                    "994e1fbe-0c4a-4fc7-bd39-5c5d361ef997"
                ]
            }
        },
        "guestTeam": {
            "clubId": "16d07540-4620-4551-92bb-158b771f5232",
            "club": "КАМАЗ",
            "scheme": "4-3-3",
            "teamColorTop": "#ffffff",
            "teamColorBottom": "#042a7f",
            "coach": {
                "name": "ВЛАДИМИР",
                "surname": "КЛОНЦАК",
                "coachType": "head coach"
            },
            "teamPlayers": {
                "captainPlayerId": "a2c6d376-b7a0-4c0d-a478-babc70d4d6ff",
                "mainTeamPlayerIds": [
                    "311aeedd-5904-4742-b4c6-19ed92c99355",
                    "c900ae63-b0c5-4860-a8de-fa60d687d08d",
                    "0d3db768-9387-42f0-85bb-ebe1f62cf14e",
                    "bdde45ba-7430-42af-8fda-3bd5e5a8313e",
                    "7e91bd16-cbb0-4bbc-becc-3feb336db13e",
                    "a205def3-1462-47e8-a400-b36c180ccb27",
                    "a2c6d376-b7a0-4c0d-a478-babc70d4d6ff",
                    "06b4f3b4-4978-45fe-b111-6b9d1f8a6b60",
                    "c1057949-9565-47db-ad20-d46b5a1ce96f",
                    "69c31446-4a62-496c-abd7-401e26a10081",
                    "90d2bc41-416a-4c47-8132-7f01560e41b1"
                ],
                "replaceTeamPlayerIds": [
                    "050990af-4f84-4a9c-90f3-a1fdec038e53",
                    "4c744d1c-06f0-4bee-ac0f-7e28bab7272e",
                    "dda22420-f021-4943-9f56-9642a1879b51",
                    "ca2d9cde-7dd0-4cc2-abc6-c7ee98d287eb",
                    "7bab224f-f326-459a-8115-10564d6af42f",
                    "809b17d6-9f7c-4263-875e-e54971910a29",
                    "e3fcb371-5722-41ae-bf53-64f9c2eda1db"
                ]
            }
        },
        "city": "Воронеж",
        "face": [
            {
                "type": 7,
                "name": "Андрей",
                "surname": "Прокопов"
            },
            {
                "type": 5,
                "name": "Артур",
                "surname": "Зайнагутдинов"
            },
            {
                "type": 2,
                "name": "Сергей",
                "surname": "Куликов"
            },
            {
                "type": 6,
                "name": "Дмитрий",
                "surname": "Колосков"
            },
            {
                "type": 1,
                "name": "Сергей",
                "surname": "Курманов"
            }
        ],
        "humidity": "25%",
        "station": "Центральный стадион профсоюзов",
        "tourNumber": "36 тур",
        "temperature": "+16°C",
        "weather": "sunny",
        "windDirection": "south-east",
        "windSpeed": "2 м/c",
        "mainTimePeriodDuration": 45,
        "additionalTimePeriodDuration": 15,
        "metaInfo": {
            "rtmpLink": "",
            "publicLink": "",
        }
    }
}

```
`PublicApiGameInfoResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameId` | `string` | Идентификатор матча |
| `status` | `number` | Статус состояния игры. |
| `dateTime` | `string` | Дата время матча в формате. В формате ISO 8601 |
| `seasonId` | `string` | Идентификатор сезона |
| `season` | `string` | Название сезона |
| `leagueId` | `string` | Идентификатор лиги |
| `league` | `string` | Название лиги |
| `hostTeam` | `PublicApiGameInfoTeamModel` | Информация по клубу хозяев |
| `guestTeam` | `PublicApiGameInfoTeamModel` | Информация по клубу гостей |
| `city` | `string` | Город проведения матча |
| `face` | `PublicApiFaceModel` | Список судей, комментатор |
| `humidity` | `string` | Влажность на стадионе |
| `station` | `string` | Стадион |
| `tourNumber` | `string` | Номер тура |
| `temperature` | `string` | Температура на стадионе |
| `weather` | `string` | Погода на стадионе |
| `windDirection` | `string` | Направление ветра |
| `windSpeed` | `string` | Скорость ветра |
| `mainTimePeriodDuration` | `string` | Время основного тайма |
| `additionalTimePeriodDuration` | `string` | Время дополнительного тайма |
| `metaInfo` | `string` | Техническая информация |
| `metaInfo.rtmpLink` | `string` | Ссылка на RTMP |
| `metaInfo.publicApi` | `string` | Ссылка на RTMP |

`PublicApiGameInfoTeamModel`
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `clubId` | `string` | Идентификатор клуба |
| `club` | `string` | Название клуба |
| `scheme` | `string` | Схема расстановки  |
| `teamColorTop` | `string` | Цвет Футболок в формате #rrggbb |
| `teamColorBottom` | `string` | Цвет Гетр (Шорт) в формате #rrggbb |
| `coach` | `string` | Тренер |
| `coach.name` | `string` | Имя тренера |
| `coach.surname` | `string` | Фамилия тренера |
| `coach.coachType` | `string` | Тип тренера |
| `teamPlayers` | `string` | Игроки команды |
| `teamPlayers.captainPlayerId` | `string` | Идентификатор капитана команды |
| `teamPlayers.mainTeamPlayerIds` | `string` | Идентификаторы игроков в основном составе. |
| `teamPlayers.replaceTeamPlayerIds` | `string` | Идентификаторы запасных игроков |

Возможные схемы расстановки (PublicApiGameInfoTeamModel.scheme):
- 3-4-3
- 3-5-2
- 4-3-3
- 4-4-2
- 4-5-1
- 5-3-2
- 5-4-1
- 4-1-4-1
- 4-2-3-1
- 4-3-2-1
- 4-3-1-2
- 4-1-2-1-2

игроки в поле teamPlayers.mainTeamPlayerIds имеют сортировку, их порядок определяется по отдаленности линни от своих ворот, а затем слева направо

![порядок сортировки](/scheme_placement.png)


`coach.coachType`
| Value | Description |
| :--- | :--- |
| `head coach` | главный тренер |
| `senior coach` | старший тренер |
| `acting head coach`  | и. о. главного тренера |
| `unknown coach`  | тренер |

`PublicApiFaceModel`
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `number` | Тип персоны |
| `name` | `string` | Имя |
| `surname` | `string` | Фамилия |

`type`
| Value | Description |
| :--- | :--- |
| 1 | Первый комментатор |
| 2 | Главный судья |
| 4 | Второй комментатор  |
| 5 | Помощник судьи |
| 6 | Помощник судьи |
| 7 | Резервный судья |

### Данные

#### /season/{seasonId} `GET`
Получение информации по сезону

##### Пример пути 
```http
GET /data/season/22d56aa8-08dc-4a38-8c9b-bce87cae57e3
```

##### Пример ответа
```javascript
{
    "result": {
        "name": "2021/2022",
        "isArchive": false
    },
    "success": true
}
```

`PublicApiDataSeason`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `name` | `string` | Название сезона |
| `isArchive` | `boolean` | true - сезон удален
false - сезон не удален |

#### /league/{leagueId} `GET`
Получение информации по лиге

##### Пример пути 
```http
GET /data/league/03c9d799-d6b6-4216-aa2a-fdda52351800
```

##### Пример ответа
```javascript
{
    "result": {
        "logoId": "cbba0110-b64f-4bca-847b-e3362f1ffa48",
        "fullName": "ФНЛ",
        "shortName": "ФНЛ",
        "isArchive": false
    },
    "success": true
}
```

`PublicApiDataLeague`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `logoId` | `string` | Идентификатор изображения лого лиги |
| `fullName` | `string` | Полное название лиги |
| `shortName` | `string` | Короткое название лиги |
| `isArchive` | `boolean` | * true - сезон удален * false - сезон не удален` |

#### /club/{clubId}
Получение информации по клубу

##### Пример пути 
```http
GET /data/club/16d07540-4620-4551-92bb-158b771f5232
```

##### Пример ответа
```javascript
{
    "result": {
        "logoId": "63ab3f16-56f8-46a5-95ff-b0f1de1345f2",
        "fullName": "КАМАЗ",
        "shortName": "КАМ",
        "city": "Набережные Челны",
        "coach": {
            "name": "ВЛАДИМИР",
            "surname": "КЛОНЦАК",
            "coachType": "head coach"
        },
        "isArchive": false
    },
    "success": true
}
```

`PublicApiDataClub`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `logoId` | `string` | Идентификатор изображения лого лиги |
| `fullName` | `string` | Полное название лиги |
| `shortName` | `string` | Короткое название лиги |
| `city` | `string` | Город клуба |
| `coach` | `string` | Тренер |
| `coach.name` | `string` | Имя тренера |
| `coach.surname` | `string` | Фамилия тренера |
| `coach.coachType` | `string` | Тип тренера |
| `isArchive` | `boolean` | * true - сезон удален * false - сезон не удален` |

`coach.coachType`
| Value | Description |
| :--- | :--- |
| `head coach` | главный тренер |
| `senior coach` | старший тренер |
| `acting head coach`  | и. о. главного тренера |
| `unknown coach`  | тренер |

#### /player/{playerId} GET
Получение информации по игроку

##### Пример пути 
```http
GET /data/player/32d764fb-b4df-41e7-ab9b-50d52cc04de4
```

##### Пример ответа
```javascript
{
    "result": {
        "logoId": "1ec3b1a7-573f-424a-8981-0f2e55edd84b",
        "number": "28",
        "name": "Руслан",
        "surname": "Магаль",
        "isArchive": false
    },
    "success": true
}
```

`PublicApiDataPlayer`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `logoId` | `string` | Идентификатор изображения лого лиги |
| `number` | `string` | Номер игрока |
| `name` | `string` | Имя |
| `surname` | `string` | Фамилия |
| `isArchive` | `boolean` | * true - сезон удален * false - сезон не удален` |

### Изображения

#### /img/{imageId}.png `GET`
Получение изображения

##### Пример пути 
```http
GET /img/1ec3b1a7-573f-424a-8981-0f2e55edd84b.png 
```

Ответ файл изображения ("application/octet-stream")

### События матча

#### /events/{gameId} `GET`
Получение данных матча

##### Параметр fromTicks
Метод предполагает использовать получение информации по частям, с помощью параметра fromTicks. Первый запрос событий по матчу отправляется без параметра fromTicks или со значением 0.
В дальнейших запросах значение fromTicks должно быть равно максимальному значение поля ticks из полученных ответов. 

| Parameter | Type | Optional/Default| Description |
| :--- | :--- | :--- | :--- |
| fromTicks | number | опционально, по умолчанию 0 | указывает тики с которых необходимо получить события |
| ignoreDeleted | boolean | опционально, по умолчанию false | при значении true исключает инкриментность данных. исчезают собития типа EventDelete и удаленные ими события  |

##### Пример пути 
```http
GET /events/22768d45-a654-4d33-9620-4c8c289edcad
GET /events/22768d45-a654-4d33-9620-4c8c289edcad?fromTicks=637877812310275451
GET /events/22768d45-a654-4d33-9620-4c8c289edcad?fromTicks=637877812310275451?ignoreDeleted=true
```

##### Пример ответа
```javascript
{
    "result": [
        {
            "type": "GameStateUpdate",
            "ticks": 637878039171422962,
            "data": {
                "gameStateNumber": 2
            }
        },
        {
            "type": "GamePeriodOpen",
            "ticks": 637878039201336201,
            "data": {
                "periodNumber": 1
            }
        }
    ],
    "success": true
}
```
##### Параметр extra

Только на тестовом стенде!

Метод предполагает использовать получение информации по частям, с помощью параметра extra. 
Параметр extra отвечает за вывод событий не только с броадкастера но и введеные через api [этим методом](#eventsgameid-post)

Первый запрос событий по матчу отправляется без параметра extra или со значением false.

| Parameter | Type | Optional/Default| Description |
| :--- | :--- | :--- | :--- |
| extra | boolean | опционально, по умолчанию false | выбирает режим отображения событий. true - полное отображение, с событиями введеными вручную. false - только с броадкастера |

##### Пример пути 
```http
GET /events/22768d45-a654-4d33-9620-4c8c289edcad
GET /events/22768d45-a654-4d33-9620-4c8c289edcad?extra=true
```

##### Пример ответа с параметром extra

```javascript
{
    "success": true,
    "result": [
        {
            "type": "GameStateUpdate",
            "ticks": 637878919500357269,
            "data": {
                "gameStateNumber": 2
            },
            "extraSource": false
        },
        {
            "type": "GamePeriodOpen",
            "ticks": 637878924773522877,
            "data": {
                "periodNumber": 1,
                "periodOpenTime": "2022-05-11T16:01:17.3522877Z"
            },
            "extraSource": false
        },
        {
            "type": "EventAdded",
            "ticks": 637878927022148287,
            "data": {
                "eventId": "1b0134a1-7fdc-4706-8ce8-04a49b459510",
                "type": "Corner",
                "gameTimer": "03:41",
                "clubId": "4082238b-9ed2-44ec-a1d2-81446340e34d",
                "isAdditionalTime": false,
                "eventSourceCreationTime": "2022-05-11T16:05:02.1080000Z"
            },
            "extraSource": false
        },
        {
            "type": "EventAdded",
            "ticks": 637925539460188929,
            "data": {
                "eventId": "62ef2e3d-36de-4766-87e4-eaeae0421cb3",
                "type": "Injury",
                "gameTimer": "55:16",
                "clubId": "4082238b-9ed2-44ec-a1d2-81446340e34d",
                "replacePlayerId": "1bcf97f4-825a-4b8e-a7c4-2a990cd03134",
                "isAdditionalTime": false,
                "eventSourceCreationTime": "2022-07-04T08:19:16.1000000Z"
            },
            "extraSource": true
        },
        {
            "type": "EventDelete",
            "ticks": 637925541920200863,
            "data": {
                "deletedEventId": "d100e741-5614-4de9-b85f-0f4eae3270ef"
            },
            "extraSource": true
        }
    ]
}
```
`PublicApiEventResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `string` | Тип события |
| `ticks` | `boolean` | Время добавления события. Значение для параметра fromTicks для запроса по частям |
| `data` | `string` | Данные события |

`PublicApiEventResponseModel.type`

| Value | Description |
| :--- | :--- |
| `EventAdded` | Добавлено игровое событие |
| `EventDelete` | Удалено игровое событие |
| `GameStateUpdate` | Изменено состояние матча |
| `GamePeriodOpen` | Открыт игровой период |
| `GamePeriodEnd` | Закрыт игровой период |
| `AddMinutes` | Добавлено время |

`PublicApiEventResponseModel.data`

| Value | Description |
| :--- | :--- |
| `EventAdded` | PublicApiEventAddModel |
| `EventDelete` | PublicApiEventDeleteModel |
| `GameStateUpdate` | PublicApiGameStateUpdateModel |
| `GamePeriodOpen` | PublicApiPeriodOpenModel |
| `GamePeriodEnd` | PublicApiPeriodEndModel |
| `AddMinutes` | PublicApiMinutesAddedModel |

`PublicApiEventAddModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `eventId` | `string` | Идентификатор игрового события |
| `type` | `boolean` | Типы игровых событий |
| `gameTimer` | `string` | Значение игрового таймера |
| `isAdditionalTime` | `string` | true - маркер создан в дополнительное время периода false - маркер создан в основное время периода |
| `clubId` | `string` | Идентификатор клуба |
| `playerId` | `string` | Идентификатор игрока Может быть не заполнен |
| `replacePlayerId` | `string` | Идентификатор игрока на замену. заполняется в случае если type = Replacement |
| `parentEventIdr` | `string` | Идентификатор родительского события. В случае добавления события гол, добавляются события удар в створ и удар. Для событий удар в створ и удар родительским идентификатором будет идентификатор события гол |
| `eventSourceCreationTime` | `string` | Время создания маркера в формате ISO 8601 |

`PublicApiEventAddModel.type`

| Value | Description |
| :--- | :--- |
| `Kick` | удар |
| `ShotOnTarget` | удар в створ |
| `Goal` | гол |
| `Offside` | офсайд |
| `Violation` | нарушение |
| `YellowCard` | предупреждение |
| `RedCard` | удаление |
| `Corner` | угловой |
| `Injury` | травма |
| `Replacement` | замена |
| `AutoGoal` | автогол |
| `Blocked shot` | заблокированный удар |
| `Post` | штанга / перекладина |
| `Out` | аут |

`PublicApiEventDeleteModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `deletedEventId` | `string` | Идентификатор удаленного игрового события |

`PublicApiGameStateUpdateModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameStateNumber` | `number` | Статус состояния игры |

`PublicApiGameStateUpdateModel.gameStateNumber`

| Value | Description |
| :--- | :--- |
| 1 | Матч запланирован |
| 2 | Матч идет |
| 3 | Матч завершен |

`PublicApiPeriodOpenModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер открывшегося периода |
| `periodOpenTime` | `string` | Время создания маркера в формате ISO 8601 |

`PublicApiPeriodOpenModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |

`PublicApiPeriodEndModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер закрывшегося периода |

`PublicApiPeriodEndModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |

`PublicApiMinutesAddedModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `addMinutes` | `number` |Количество добавленных минут может быть 0 в случае, если предыдущее значение поставили по ошибке |


#### /events/{gameId} `POST`
Внимание! Только на тестовом стенде

Добавление события через api.
Будет проставлена метка источника "extraSource": true что значит, что метка проставлена не через броадкастер.

```http
POST /events/22768d45-a654-4d33-9620-4c8c289edcad/
```

##### Тело POST запроса raw-json

Пример заполнения тела:

```javascript
{
    "type": "Replacement",
    "gameMinutes": 10,
    "gameSeconds": 12,
    "clubId": "4082238b-9ed2-44ec-a1d2-81446340e34d",
    "playerId": "cc5d4eb9-adaa-4dac-a982-b906b3f7c7f2",
    "replacePlayerId": "1bcf97f4-825a-4b8e-a7c4-2a990cd03134",
    "parentMarkerId": null,
    "creationTicks": 1656938756957
}
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": "92e563a2-762d-4657-a59b-5abab1b61e4a"
}
```

`PublicApiAddEventRequestModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `string` | Типы игровых событий |
| `gameMinutes` | `boolean` | Значение игрового таймера - минуты |
| `gameSeconds` | `string` | Значение игрового таймера - секунды |
| `clubId` | `string` | Идентификатор клуба |
| `playerId` | `string` | Идентификатор игрока Может быть не заполнен |
| `replacePlayerId` | `string` | Идентификатор игрока на замену. заполняется в случае если type = Replacement |
| `parentMarkerId` | `string` | Идентификатор родительского события. В случае добавления события гол, добавляются события удар в створ и удар. Для событий удар в створ и удар родительским идентификатором будет идентификатор события гол |
| `creationTicks` | `number` | Время создания маркера в формате ISO 8601 |

`PublicApiAddEventRequestModel.type`
| Value | Description |
| :--- | :--- |
| `Kick` | удар |
| `ShotOnTarget` | удар в створ |
| `Goal` | гол |
| `Assist` | ассист |
| `Offside` | офсайд |
| `Violation` | нарушение |
| `YellowCard` | предупреждение |
| `RedCard` | удаление |
| `Corner` | угловой |
| `Injury` | травма |
| `Replacement` | замена |
| `AutoGoal` | автогол |
| `Blocked shot` | заблокированный удар |
| `Post` | штанга / перекладина |
| `Out` | аут |
| `AppointedPenalty` | пенальти |

#### /events/{gameId}/{eventId} `DELETE`
Удаление события через api. 

```http
delete /events/22768d45-a654-4d33-9620-4c8c289edcad/74a41d9f-d101-42e5-80fe-33ae6a95f11b
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": "d100e741-5614-4de9-b85f-0f4eae3270ef"
}
```


### РПЛ

#### /RPL/Games `GET`
Получение списка доступных матчей

```http
GET /rpl/games
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": [
        {
            "gameId": 15025,
            "status": 1,
            "dateTime": "2023-02-11T17:00:00.0000000Z",
            "seasonId": 24,
            "season": "2022/2023",
            "leagueId": 122,
            "league": "Winline Зимний кубок РПЛ",
            "clubHostId": 11,
            "clubHost": "ФК «Ростов» Ростов-на-Дону",
            "clubGuestId": 584,
            "clubGuest": "ФК «Краснодар» Краснодар"
        },
    ]
}
```

`PublicApiGameListItemResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameId` | `long` | Идентификатор матча из базы РПЛ |
| `status` | `number` | Статус состояния игры. |
| `dateTime` | `string` | Дата время матча в формате. В формате ISO 8601 |
| `seasonId` | `long` | Идентификатор сезона из базы РПЛ |
| `season` | `string` | Название сезона |
| `leagueId` | `long` | Идентификатор лиги из базы РПЛ |
| `league` | `string` | Название лиги |
| `clubHostId` | `long` | Идентификатор клуба хозяев из базы РПЛ |
| `clubHost` | `string` | Название клуба хозяев |
| `clubGuestId` | `long` | Идентификатор клуба гостей из базы РПЛ |
| `clubGuest` | `string` | Название клуба гостей |

`status`
| Value | Description |
| :--- | :--- |
| 1 | Матч запланирован |
| 2 | Матч идет |
| 3 | Матч завершен |

#### /RPL/Games/{gameId} `GET`
Получение данных матча

##### Пример пути 
```http
GET /rpl/games/15025
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": {
        "gameId": 15025,
        "status": 1,
        "dateTime": "2023-02-11T17:00:00.0000000Z",
        "metaInfo": {},
        "seasonId": 24,
        "season": "2022/2023",
        "leagueId": 122,
        "league": "Winline Зимний кубок РПЛ",
        "mainTimePeriodDuration": 45,
        "additionalTimePeriodDuration": 15,
        "face": [],
        "hostTeam": {
            "clubId": 11,
            "club": "ФК «Ростов» Ростов-на-Дону",
            "teamColorTop": "#ffffff",
            "teamColorBottom": "#ffffff",
            "coach": {
                "name": "Валерий",
                "surname": "Карпин",
                "coachType": "Главный тренер"
            },
            "teamPlayers": {
                "mainTeamPlayerIds": [],
                "replaceTeamPlayerIds": []
            }
        },
        "guestTeam": {
            "clubId": 584,
            "club": "ФК «Краснодар» Краснодар",
            "teamColorTop": "#ffffff",
            "teamColorBottom": "#ffffff",
            "coach": {
                "name": "Александр",
                "surname": "Сторожук",
                "coachType": "Главный тренер"
            },
            "teamPlayers": {
                "mainTeamPlayerIds": [],
                "replaceTeamPlayerIds": []
            }
        }
    }
}

```
`PublicApiGameInfoResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameId` | `long` | Идентификатор матча из базы РПЛ |
| `status` | `number` | Статус состояния игры. |
| `dateTime` | `string` | Дата время матча в формате. В формате ISO 8601 |
| `seasonId` | `long` | Идентификатор сезона из базы РПЛ |
| `season` | `string` | Название сезона |
| `leagueId` | `long` | Идентификатор лиги из базы РПЛ |
| `league` | `string` | Название лиги |
| `hostTeam` | `PublicApiGameInfoTeamModel` | Информация по клубу хозяев |
| `guestTeam` | `PublicApiGameInfoTeamModel` | Информация по клубу гостей |
| `city` | `string` | Город проведения матча |
| `face` | `PublicApiFaceModel` | Список судей, комментатор |
| `humidity` | `string` | Влажность на стадионе |
| `station` | `string` | Стадион |
| `tourNumber` | `string` | Номер тура |
| `temperature` | `string` | Температура на стадионе |
| `weather` | `string` | Погода на стадионе |
| `windDirection` | `string` | Направление ветра |
| `windSpeed` | `string` | Скорость ветра |
| `mainTimePeriodDuration` | `string` | Время основного тайма |
| `additionalTimePeriodDuration` | `string` | Время дополнительного тайма |
| `metaInfo` | `string` | Техническая информация |
| `metaInfo.rtmpLink` | `string` | Ссылка на RTMP |
| `metaInfo.publicApi` | `string` | Ссылка на RTMP |

`PublicApiGameInfoTeamModel`
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `clubId` | `long` | Идентификатор клубаиз базы РПЛ  |
| `club` | `string` | Название клуба |
| `scheme` | `string` | Схема расстановки  |
| `teamColorTop` | `string` | Цвет Футболок в формате #rrggbb |
| `teamColorBottom` | `string` | Цвет Гетр (Шорт) в формате #rrggbb |
| `coach` | `string` | Тренер |
| `coach.name` | `string` | Имя тренера |
| `coach.surname` | `string` | Фамилия тренера |
| `coach.coachType` | `string` | Тип тренера |
| `teamPlayers` | `string` | Игроки команды |
| `teamPlayers.captainPlayerId` | `long` | Идентификатор капитана команды |
| `teamPlayers.mainTeamPlayerIds` | `long` | Идентификаторы игроков в основном составе. |
| `teamPlayers.replaceTeamPlayerIds` | `long` | Идентификаторы запасных игроков |

игроки в поле teamPlayers.mainTeamPlayerIds имеют сортировку, их порядок определяется по отдаленности линни от своих ворот, а затем слева направо


![порядок сортировки](/scheme_placement.png)


`coach.coachType`
| Value | Description |
| :--- | :--- |
| `head coach` | главный тренер |
| `senior coach` | старший тренер |
| `acting head coach`  | и. о. главного тренера |
| `unknown coach`  | тренер |

`PublicApiFaceModel`
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `number` | Тип персоны |
| `name` | `string` | Имя |
| `surname` | `string` | Фамилия |

`type`
| Value | Description |
| :--- | :--- |
| 1 | Первый комментатор |
| 2 | Главный судья |
| 4 | Второй комментатор  |
| 5 | Помощник судьи |
| 6 | Помощник судьи |
| 7 | Резервный судья |


#### /rpl/events/{gameId} `GET`
Получение событий матча

##### Параметр fromTicks
Метод предполагает использовать получение информации по частям, с помощью параметра fromTicks. Первый запрос событий по матчу отправляется без параметра fromTicks или со значением 0.
В дальнейших запросах значение fromTicks должно быть равно максимальному значение поля ticks из полученных ответов. 

| Parameter | Type | Optional/Default| Description |
| :--- | :--- | :--- | :--- |
| fromTicks | number | опционально, по умолчанию 0 | указывает тики с которых необходимо получить события |
| ignoreDeleted | boolean | опционально, по умолчанию false | при значении true исключает инкриментность данных. исчезают собития типа EventDelete и удаленные ими события  |

##### Пример пути 
```http
GET /rpl/events/15025
GET /rpl/events/15025?fromTicks=637877812310275451
GET /rpl/events/15025?fromTicks=637877812310275451&ignoreDeleted=true
```

##### Пример ответа
```javascript
{
    "result": [
        {
            "type": "GameStateUpdate",
            "ticks": 637878039171422962,
            "data": {
                "gameStateNumber": 2
            }
        },
        {
            "type": "GamePeriodOpen",
            "ticks": 637878039201336201,
            "data": {
                "periodNumber": 1
            }
        }
    ],
    "success": true
}
```

`PublicApiEventResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `string` | Тип события |
| `ticks` | `boolean` | Время добавления события. Значение для параметра fromTicks для запроса по частям |
| `data` | `string` | Данные события |

`PublicApiEventResponseModel.type`

| Value | Description |
| :--- | :--- |
| `EventAdded` | Добавлено игровое событие |
| `EventDelete` | Удалено игровое событие |
| `GameStateUpdate` | Изменено состояние матча |
| `GamePeriodOpen` | Открыт игровой период |
| `GamePeriodEnd` | Закрыт игровой период |
| `AddMinutes` | Добавлено время |

`PublicApiEventResponseModel.data`

| Value | Description |
| :--- | :--- |
| `EventAdded` | PublicApiEventAddModel |
| `EventDelete` | PublicApiEventDeleteModel |
| `GameStateUpdate` | PublicApiGameStateUpdateModel |
| `GamePeriodOpen` | PublicApiPeriodOpenModel |
| `GamePeriodEnd` | PublicApiPeriodEndModel |
| `AddMinutes` | PublicApiMinutesAddedModel |

`PublicApiEventAddModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `eventId` | `string` | Идентификатор игрового события |
| `type` | `boolean` | Типы игровых событий |
| `gameTimer` | `string` | Значение игрового таймера |
| `isAdditionalTime` | `string` | true - маркер создан в дополнительное время периода false - маркер создан в основное время периода |
| `clubId` | `long` | Идентификатор клуба из базы РПЛ|
| `playerId` | `long` | Идентификатор игрока из базы РПЛ. Может быть не заполнен |
| `replacePlayerId` | `long` | Идентификатор игрока на замену из базы РПЛ. заполняется в случае если type = Replacement |
| `parentEventIdr` | `string` | Идентификатор родительского события. В случае добавления события гол, добавляются события удар в створ и удар. Для событий удар в створ и удар родительским идентификатором будет идентификатор события гол |
| `eventSourceCreationTime` | `string` | Время создания маркера в формате ISO 8601 |
| `varCode` | `int` | Код события VAR |

`PublicApiEventAddModel.type`

| Value | Description |
| :--- | :--- |
| `Kick` | удар |
| `ShotOnTarget` | удар в створ |
| `Goal` | гол |
| `Assist` | ассист |
| `Offside` | офсайд |
| `Violation` | нарушение |
| `YellowCard` | предупреждение |
| `RedCard` | удаление |
| `Corner` | угловой |
| `Injury` | травма |
| `Replacement` | замена |
| `AutoGoal` | автогол |
| `AppointedPenalty` | пенальти |
| `VAR` | Событие VAR |

`PublicApiEventAddModel.varCode`
| Value | Description |
| :--- | :--- |
| 202 | Решение подтверждено - гол |
| 302 | Решение подтверждено - нет гола |
| 402 | Решение подтверждено - пенальти |
| 502 | Решение подтверждено - нет пенальти |
| 602 | Решение подтверждено - красная карточка |
| 702 | Решение подтверждено - нет красной карточки |

`PublicApiEventDeleteModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `deletedEventId` | `string` | Идентификатор удаленного игрового события |

`PublicApiGameStateUpdateModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameStateNumber` | `number` | Статус состояния игры |

`PublicApiGameStateUpdateModel.gameStateNumber`

| Value | Description |
| :--- | :--- |
| 1 | Матч запланирован |
| 2 | Матч идет |
| 3 | Матч завершен |

`PublicApiPeriodOpenModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер открывшегося периода |
| `periodOpenTime` | `string` | Время создания маркера в формате ISO 8601 |

`PublicApiPeriodOpenModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |

`PublicApiPeriodEndModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер закрывшегося периода |

`PublicApiPeriodEndModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |

`PublicApiMinutesAddedModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `addMinutes` | `number` |Количество добавленных минут может быть 0 в случае, если предыдущее значение поставили по ошибке |

#### /rpl/flat/{gameId} `GET`
Получение событий матча в "Плоском" режиме: нет дочерних маркеров (к примеру марекер "Удар" создавался автоматически при маркере "Гол" - теперь создается только маркер "гол" и точка.) 
Сортировка событий строго по игровому времени


##### Пример пути 
```http
GET /rpl/flat/15025
```

##### Пример ответа
```javascript
{
    "result": [
        {
            "type": "GameStateUpdate",
            "ticks": 637878039171422962,
            "data": {
                "gameStateNumber": 2
            }
        },
        {
            "type": "GamePeriodOpen",
            "ticks": 637878039201336201,
            "data": {
                "periodNumber": 1
            }
        }
    ],
    "success": true
}
```

`PublicApiEventResponseModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `string` | Тип события |
| `data` | `string` | Данные события |

`PublicApiEventResponseModel.type`

| Value | Description |
| :--- | :--- |
| `EventAdded` | Добавлено игровое событие |
| `GameStateUpdate` | Изменено состояние матча |
| `GamePeriodOpen` | Открыт игровой период |
| `GamePeriodEnd` | Закрыт игровой период |
| `AddMinutes` | Добавлено время |

`PublicApiEventResponseModel.data`

| Value | Description |
| :--- | :--- |
| `EventAdded` | PublicApiEventAddModel |
| `GameStateUpdate` | PublicApiGameStateUpdateModel |
| `GamePeriodOpen` | PublicApiPeriodOpenModel |
| `GamePeriodEnd` | PublicApiPeriodEndModel |
| `AddMinutes` | PublicApiMinutesAddedModel |

`PublicApiEventAddModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `eventId` | `string` | Идентификатор игрового события |
| `type` | `boolean` | Типы игровых событий |
| `gameTimer` | `string` | Значение игрового таймера |
| `isAdditionalTime` | `string` | true - маркер создан в дополнительное время периода false - маркер создан в основное время периода |
| `clubId` | `long` | Идентификатор клуба из базы РПЛ|
| `playerId` | `long` | Идентификатор игрока из базы РПЛ. Может быть не заполнен |
| `replacePlayerId` | `long` | Идентификатор игрока на замену из базы РПЛ. заполняется в случае если type = Replacement |
| `eventSourceCreationTime` | `string` | Время создания маркера в формате ISO 8601 |
| `isPenaltyRealized` | `boolean` | true - пенальти реализовано (гол забит) false - пенальти нереализовано (промах) |
| `assistPlayerId` | `long` | Идентификатор игрока из базы РПЛ, которые передал голевую передачу (assist). Может быть не заполнен |
| `varCode` | `int` | Код события VAR |

`PublicApiEventAddModel.type`

| Value | Description |
| :--- | :--- |
| `Kick` | удар |
| `ShotOnTarget` | удар в створ |
| `Goal` | гол |
| `Assist` | ассист (голевая передача) |
| `Offside` | офсайд |
| `Violation` | нарушение |
| `YellowCard` | предупреждение |
| `RedYellowCard` | Вторая желтая карточка, повлекшая удаление |
| `RedCard` | удаление |
| `Corner` | угловой |
| `Injury` | травма |
| `Replacement` | замена |
| `AutoGoal` | автогол |
| `AppointedPenalty` | пенальти |
| `VAR` | Событие VAR |

`PublicApiEventAddModel.varCode`
| Value | Description |
| :--- | :--- |
| 202 | Решение подтверждено - гол |
| 302 | Решение подтверждено - нет гола |
| 402 | Решение подтверждено - пенальти |
| 502 | Решение подтверждено - нет пенальти |
| 602 | Решение подтверждено - красная карточка |
| 702 | Решение подтверждено - нет красной карточки |

`PublicApiGameStateUpdateModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `gameStateNumber` | `number` | Статус состояния игры |

`PublicApiGameStateUpdateModel.gameStateNumber`

| Value | Description |
| :--- | :--- |
| 1 | Матч запланирован |
| 2 | Матч идет |
| 3 | Матч завершен |

`PublicApiPeriodOpenModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер открывшегося периода |
| `periodOpenTime` | `string` | Время создания маркера в формате ISO 8601 |

`PublicApiPeriodOpenModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |

`PublicApiPeriodEndModel`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `periodNumber` | `number` | Номер закрывшегося периода |

`PublicApiPeriodEndModel.periodNumber`

| Value | Description |
| :--- | :--- |
| 1 | первый период |
| 2 | второй период |
| 3 | первый дополнительный период |
| 4 | второй дополнительный период |
| 5 | серия пенальти |



#### /rpl/statistic/{gameId} `GET`
Получение лайв статистики 

##### Пример пути 
```http
GET /rpl/statistic/15025
```

##### Пример ответа
```javascript
{
    "success": true,
    "result": {
        "goal": {
            "host": 1,
            "guest": 1
        },
        "kick": {
            "host": 11,
            "guest": 7
        },
        "shotOnTarget": {
            "host": 2,
            "guest": 4
        },
        "ballPossession": {
            "host": 58,
            "guest": 42
        },
        "violation": {
            "host": 22,
            "guest": 17
        },
        "yellowCard": {
            "host": 2,
            "guest": 3
        },
        "redCard": {
            "host": 0,
            "guest": 0
        },
        "offside": {
            "host": 1,
            "guest": 0
        },
        "corner": {
            "host": 6,
            "guest": 3
        }
    }
}
```

`PublicApiStatisticInfo`

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `goal` | PublicApiStatisticInfoLine | Голы |
| `kick` | PublicApiStatisticInfoLine | Удары |
| `shotOnTarget` | PublicApiStatisticInfoLine | Удары в створ  |
| `ballPossession` | PublicApiStatisticInfoLine | Владение мячем(%) |
| `violation` | PublicApiStatisticInfoLine | Нарушения |
| `yellowCard` | PublicApiStatisticInfoLine | Предупреждения |
| `redCard` | PublicApiStatisticInfoLine | Удаления |
| `offside` | PublicApiStatisticInfoLine | Офсайды |
| `corner` | PublicApiStatisticInfoLine | Угловые |

`PublicApiStatisticInfoLine`
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `host` | `int` | Показатель хозяев |
| `guest` | `int` | Показатель гостей |
